//
//  UITextFieldExt.swift
//  Xapp
//
//  Created by Javad faghih on 10/6/1399 AP.
//

import UIKit

extension UITextField {
    func disableAutoFill() {
        if #available(iOS 12, *) {
            textContentType = .oneTimeCode
        } else {
            textContentType = .init(rawValue: "")
        }
    }
}
