//
//  SceneDelegate.swift
//  Xapp
//
//  Created by Javad faghih on 9/29/1399 AP.
//

import UIKit
import Firebase


class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var authlistener: AuthStateDidChangeListenerHandle?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        resetBudge()
        autoLogIn()
        
        guard let _ = (scene as? UIWindowScene) else { return }
    }

    func sceneDidDisconnect(_ scene: UIScene) {
     resetBudge()
        
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        LocationManager.shared.startUpdating()
        resetBudge()
    }

    func sceneWillResignActive(_ scene: UIScene) {
   
        
        LocationManager.shared.stopUpdating()

    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
        resetBudge()
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
      
        LocationManager.shared.stopUpdating()
        resetBudge()
    }

     //MARK: - Auto Login

    func autoLogIn() {
        
        authlistener = Auth.auth().addStateDidChangeListener({ (auth, user) in
            Auth.auth().removeStateDidChangeListener(self.authlistener!)
            
            if user != nil && userDefaults.object(forKey: kCURRENTUSER) != nil {
               
                DispatchQueue.main.async {
                    self.goToApp()

                }
                
            }
        })
    }
    
     //MARK: - Navigation
    
    func goToApp() {
        
        let storyBoard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MainVC") as UITabBarController
        
        window?.rootViewController = storyBoard
    }
        private func resetBudge() {
            
            UIApplication.shared.applicationIconBadgeNumber = 0
            
        }
    }

