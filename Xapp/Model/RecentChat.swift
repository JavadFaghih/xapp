//
//  RecentChat.swift
//  Xapp
//
//  Created by Javad faghih on 10/10/1399 AP.
//

import UIKit
import FirebaseFirestoreSwift


struct RecentChat: Codable {
    
    var id = ""
    var chatRoomId = ""
    var senderId = ""
    var senderName = ""
    var reciverId = ""
    var reciverName = ""
    @ServerTimestamp var date = Date()
    var memberIds = [""]
    var lastMessage = ""
    var underCounter = 0
    var avatarLink = ""
 
}

