//
//  User.swift
//  Xapp
//
//  Created by Javad faghih on 9/30/1399 AP.
//

import Foundation
import Firebase
import FirebaseFirestoreSwift

struct User: Codable, Equatable {
    var id = ""
    var username: String
    var email: String
    var pushID = ""
    var avatarLink = ""
    var status: String
    
    
    static var currentID: String {
        return Auth.auth().currentUser!.uid
    }
    
    
    static var currentUser: User? {
        if Auth.auth().currentUser != nil {
            if let dictionary = UserDefaults.standard.data(forKey: kCURRENTUSER) {
              
                let decoder = JSONDecoder()
                
                do {
                    let object = try? decoder.decode(User.self, from: dictionary)
                    return object
                } catch {
                    print("error decoding user from user defaults,", error.localizedDescription)
                }
            }
            
        }
        return nil
    }
    
    static func == (lhs: User, rhs: User) -> Bool {
        lhs.id == rhs.id
        
        
    }
}


func saveUserlocaly(user: User) {
    
    let encoder = JSONEncoder()
    
    do {
      let data = try encoder.encode(user)
        UserDefaults.standard.set(data, forKey: kCURRENTUSER)
    } catch {
print("error saving localy")
    }

}


func createDummyUser() {
    
    let names = ["Javad", "Mohammad", "Ali", "kamran", "Poor Sina", "najaf", "Hashem", "hatam", "Taregh"]
    
    var imageIndex = 1
    var userIndex = 1
    
    for i in 0..<5 {
        
        let id = UUID().uuidString

        
        let fileDirectory = "Avatars/" + "\(id)" + ".jpg"
        
        FileStorage.uploadImage(UIImage(named: "user\(imageIndex)")!, directory: fileDirectory) { (avarLink) in
            
            let user = User(id: id, username: names[userIndex], email: names[i] + "@gmail.com", pushID: "", avatarLink: avarLink ?? "", status: "no status")
            
            
            FireBaseUserlistener.shared.saveUserToFirestore(user)
            userIndex += 1
            
        }
        imageIndex += 1
        if imageIndex == 5 {
            imageIndex = 1
        }
        
    }
    
    
    
}

