//
//  AudioRecorder.swift
//  Xapp
//
//  Created by Javad faghih on 10/26/1399 AP.
//

import Foundation
import AVFoundation

class AudioRecorder: NSObject, AVAudioRecorderDelegate {
    
    static let shared = AudioRecorder()
    
    var recordingSession: AVAudioSession!
    var audiorecorder: AVAudioRecorder!
    var isAudioRecordingGranted: Bool!
   
    private override init() {
        super.init()
        
        checkForRecordPermission()
    }
    
    func checkForRecordPermission() {
        switch AVAudioSession.sharedInstance().recordPermission {
        
        
        
        case .undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission { (isAllowed) in
                
                self.isAudioRecordingGranted = isAllowed
                
            }
        case .denied:
            isAudioRecordingGranted = false
            break
        case .granted:
            isAudioRecordingGranted = true
            break
        @unknown default:
            break
        }
    }

    func setupRecorder() {
        
        if isAudioRecordingGranted {
            
            recordingSession = AVAudioSession.sharedInstance()
            
            do {
                try recordingSession.setCategory(.playAndRecord, mode: .default)
                try recordingSession.setActive(true)
            } catch {
                print("error setting up audio recorder \(error.localizedDescription)")
            }
        }
    }
    
    func startRecording(fileName: String) {
        
        let audioFileName = getDocumentsURL().appendingPathComponent(fileName + ".m4a", isDirectory: false)
        
        let settings = [
            AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
        ]
        
        do {
            
            audiorecorder = try AVAudioRecorder(url: audioFileName, settings: settings)
            audiorecorder.delegate = self
            audiorecorder.record()
            
        } catch {
            print("error recording \(error.localizedDescription)")
            finishrecording()
        }
        
    }
    
    func finishrecording() {
        
        if audiorecorder != nil {
            
            audiorecorder.stop()
            audiorecorder = nil
        }
        
        
    }
    
}
