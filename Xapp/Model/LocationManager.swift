//
//  LocationManager.swift
//  Xapp
//
//  Created by Javad faghih on 10/25/1399 AP.
//

import Foundation
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate{
    
    
    static let shared = LocationManager()
 
    var locationManager: CLLocationManager?
    var currentLocation: CLLocationCoordinate2D?
    
    
    private  override init() {
        super.init()
        requsetLocationAcces()
    }
    
    func requsetLocationAcces() {
    print("auth location manager")
        if locationManager == nil {
            locationManager = CLLocationManager()
            locationManager!.delegate = self
            locationManager!.desiredAccuracy = kCLLocationAccuracyBest
            locationManager!.requestWhenInUseAuthorization()
        } else {
            
            print("we have location manager")
            
        }
        }
    
        func startUpdating() {
            
            locationManager!.startUpdatingLocation()
            
        }
        
        func stopUpdating() {
            
            if locationManager != nil {
                
                locationManager!.stopUpdatingLocation()
                
            }
            
            
        }
        
     //MARK: - Delegate
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("failed to get location")
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations.last!.coordinate
    }
    
    func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        
        if manager.authorizationStatus == .notDetermined {
            
            self.locationManager!.requestWhenInUseAuthorization()
        }
        
        
    }
    
}
