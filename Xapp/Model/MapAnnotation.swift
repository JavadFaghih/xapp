//
//  MapAnnotation.swift
//  Xapp
//
//  Created by Javad faghih on 10/26/1399 AP.
//

import Foundation
import MapKit

class Mapannotation: NSObject, MKAnnotation {
  
    let title: String?
    var coordinate: CLLocationCoordinate2D
    
    
    
    init(title: String?, coordinate: CLLocationCoordinate2D) {
        
        self.title = title
        self.coordinate = coordinate
    }
    
    
    
}
