//
//  IncomingMessage.swift
//  Xapp
//
//  Created by Javad faghih on 10/20/1399 AP.
//

import Foundation
import MessageKit
import CoreLocation

class IncomingMessage {
    
    var messageCollectionView: MessagesViewController
    
    init(_collectionView: MessagesViewController) {
        messageCollectionView = _collectionView
    }
    
     //MARK: - CreateMessages
    
    func createMessage(localMesaage: LocalMessage) -> MKMessage?{
        
        let mkMessage = MKMessage(message: localMesaage)
        
        if localMesaage.type == kPHOTO {
            
            let photoItem = PhotoMessage(path: localMesaage.pictureUrl)
            
            mkMessage.photoItem = photoItem
            mkMessage.kind = MessageKind.photo(photoItem)
   
            FileStorage.downloadImage(imageUrl: localMesaage.pictureUrl) { (image) in
                
                mkMessage.photoItem?.image = image
                self.messageCollectionView.messagesCollectionView.reloadData()
                
            }
        }
   
        if localMesaage.type == kVIDEO {
         
            FileStorage.downloadImage(imageUrl: localMesaage.pictureUrl) { (tumbnail) in
                
                FileStorage.downloadVideo(videoLink: localMesaage.videoUrl) { (readToPlay, fileName) in
                
                let videoURL = URL(fileURLWithPath: fileInDocumentsDirectory(fileName: fileName))
                
                let videoItem = VideoMessage(url: videoURL)
                
                    mkMessage.videoItem = videoItem
                    mkMessage.kind = MessageKind.video(videoItem)
                    
                }
                mkMessage.videoItem?.image = tumbnail
                self.messageCollectionView.messagesCollectionView.reloadData()
            }
        }
        
        
        if localMesaage.type == kLOCATION {
        
        
            let locationItem = LocationMessage(location: CLLocation(latitude: localMesaage.latitude, longitude: localMesaage.longitude))
        
            mkMessage.kind = MessageKind.location(locationItem)
            mkMessage.locationItem = locationItem
       
        }
        
        if localMesaage.type == kAUDIO {
            
            let audioMessage = Audiomessage(duration: Float(localMesaage.audioDuration))
            
            mkMessage.audioItem = audioMessage
            mkMessage.kind = MessageKind.audio(audioMessage)
            
            
            FileStorage.downloadAudio(audioLink: localMesaage.audioUrl) { (fileName) in
                
                let audioURL = URL(fileURLWithPath: fileInDocumentsDirectory(fileName: fileName))
                
                mkMessage.audioItem?.url = audioURL
                
            }
            self.messageCollectionView.messagesCollectionView.reloadData()
        }
        
        
        
        return mkMessage
    }
}
