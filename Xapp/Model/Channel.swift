//
//  Channel.swift
//  Xapp
//
//  Created by Javad faghih on 10/27/1399 AP.
//

import Foundation
import FirebaseFirestoreSwift
import Firebase

struct Channel: Codable {
    
    var id = ""
    var name = ""
    var adminId = ""
    var memberIds = [""]
    var avatarLink = ""
    var aboutChannel = ""
   @ServerTimestamp var createDate = Date()
  @ServerTimestamp  var lastMessageDate = Date()
    
    enum CodingKeys: String, CodingKey {
        
        case id
        case name
        case adminId
        case memberIds
        case avatarLink
        case aboutChannel
        case createDate
        case lastMessageDate = "date"
        
        
    }
    
}
