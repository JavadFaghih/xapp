//
//  VideoMessage.swift
//  Xapp
//
//  Created by Javad faghih on 10/25/1399 AP.
//

import Foundation
import MessageKit

class VideoMessage: NSObject, MediaItem {
 
    var url: URL?
    
    var image: UIImage?
    
    var placeholderImage: UIImage
    
    var size: CGSize
    
    init(url: URL?) {
        self.url = url
        self.placeholderImage = UIImage(named: "photoPlaceholder")!
        self.size = CGSize(width: 240, height: 240)
    }
    
    
}
