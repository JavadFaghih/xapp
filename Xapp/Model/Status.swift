//
//  Status.swift
//  Xapp
//
//  Created by Javad faghih on 10/8/1399 AP.
//

import Foundation

enum Status: String, CaseIterable {
    
    case avilable = "Available"
    case Busy = "Busy"
    case atSchool = "At The School"
    case atTheMovies = "At the Movies"
    case AtWork = "at Work"
    case batteryAboutToDie = "battery About to die"
    case cantTalk = "Can't Talk"
    case inMeeting = "In a meeting"
    case atTheGym = "at The gym"
    case sleeping = "sleeping"
    case UrgentCallsOnly = "urgent Calls  Only"
    
}
