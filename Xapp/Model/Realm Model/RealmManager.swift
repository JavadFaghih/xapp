//
//  RealmManager.swift
//  Xapp
//
//  Created by Javad faghih on 10/17/1399 AP.
//

import Foundation
import RealmSwift

class RealmManager {
    
    static let shared = RealmManager()
    
    let realm = try! Realm()
    
   private init() { }
    
    func saveToRealm<T: Object>(_ object: T) {
        
        do {
            try realm.write {
                realm.add(object, update: .all)
                
            }
        }
        catch {
            print("error saving to Realm...")
            
        }
        
        
    }
    
    func deleteFromRealm<T: Object>(_ object: T) {
        
        do {
            try realm.write {
                realm.delete(object)
                
            }
            
        }
        catch {
           print("error deleting from realm...")
        }
        
        
        
    }
    
}


