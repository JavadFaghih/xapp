//
//  LoginVC.swift
//  Xapp
//
//  Created by Javad faghih on 9/29/1399 AP.
//

import UIKit
import ProgressHUD


class LoginVC: UIViewController {

    //MARK: - IBOutlets
   
    //Labels
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var passwordLbl: UILabel!
    @IBOutlet weak var confirmPasswordLbl: UILabel!
    @IBOutlet weak var signupLbl: UILabel!
    
    //Textfields
    @IBOutlet weak var emailTextfields: UITextField!
    @IBOutlet weak var passwordTextfields: UITextField!
    @IBOutlet weak var confirmpasswordTextfields: UITextField!
    //Buttons
    @IBOutlet weak var loginButtons: UIButton!
    @IBOutlet weak var singupBtn: UIButton!
    @IBOutlet weak var resendEmailBtn: UIButton!
    //Views
    @IBOutlet weak var repeatPasswordLinView: UIView!
    
    
    //Mark: - Vars
    
    var isLogin: Bool = false
    
    
    //MARK: - View LifeCycles

    override func viewDidLoad() {
        super.viewDidLoad()

        setupTextFieldDelegates()
     
        setupBackGroundTap()
        passwordTextfields.textContentType = .oneTimeCode
    }
    
    override func viewWillAppear(_ animated: Bool) {
      //  updateUIFor(login: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
      //  updateUIFor(login: true)
    }
    
    //MARK: - IBActions
    @IBAction func loginButtonPressed(_ sender: UIButton) {
        
        if isDataInputedFor(type: isLogin ? "registration" : "login") {
            
            //log in or register
            print("have data for log in register...")
            
            isLogin ? registerUser() : loginUser()
            
        }
        else {
            ProgressHUD.showFailed("All Fields are required")
            
        }
        
        
    }
    
    @IBAction func resetEmailButtonPressed(_ sender: Any) {
        
        if isDataInputedFor(type: "password") {
            //resend verfication Email
        resendVerificationEmail()
            print("have data for resend email")
        
        }
        else {
            ProgressHUD.showFailed("")
        }
        
    }
    @IBAction func forgetPasswordButtonPressed(_ sender: Any) {
        
        if isDataInputedFor(type: "password") {
            // reset password
            resetPassword()
            print("have data for resset password...")         }
        else {
            
            ProgressHUD.showFailed("email is required")
        }
        
        
    }
    @IBAction func signupButtonPressed(_ sender: UIButton) {
        
        updateUIFor(login: isLogin)
        
        isLogin.toggle()
    }
    
     //MARK: - 
    
    
    private func setupTextFieldDelegates() {
        
        emailTextfields.addTarget(self, action: #selector(changelabelplaceholder(_:)), for: .editingChanged)
        passwordTextfields.addTarget(self, action: #selector(changelabelplaceholder(_:)), for: .editingChanged)
        confirmpasswordTextfields.addTarget(self, action: #selector(changelabelplaceholder(_:)), for: .editingChanged)
        
    }
    
    
    private func setupBackGroundTap() {
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(backgroundTap))
        
        view.addGestureRecognizer(tap)
    }
    
    @objc func backgroundTap() {
        
        view.endEditing(true)
        
    }
    
    
    //MARK: - Animation
    
    private func updateUIFor(login: Bool) {
        
        loginButtons.setImage(UIImage(named: login ? "loginBtn" : "registerBtn"), for: .normal)
        singupBtn.setTitle(login ? "Singn Up" : "Sign in", for: .normal)
        signupLbl.text = login ? "Don't have an account?" : "Have an account?"
        
        UIView.animate(withDuration: 0.5) {
            self.confirmPasswordLbl.isHidden = login
            self.confirmpasswordTextfields.isHidden = login
            self.repeatPasswordLinView.isHidden = login
            
        }
        
        
    }
    
    
    
   @objc private func changelabelplaceholder(_ textField: UITextField) {
        
        switch textField {
        case emailTextfields:
            emailLbl.text =  textField.hasText ? "Email" : ""
        case passwordTextfields:
            passwordLbl.text = passwordTextfields.hasText ? "Password" : ""
        default:
            confirmPasswordLbl.text = confirmpasswordTextfields.hasText ? "Confirm Password" : ""
        }
        
    }
    
    
    //MARK: - helpers
    
    private func isDataInputedFor(type: String) -> Bool{
        
        switch type {
        case "login":
            return !emailTextfields.text!.isEmpty && !passwordTextfields.text!.isEmpty
        case "registration":
            return !emailTextfields.text!.isEmpty && !passwordTextfields.text!.isEmpty && !confirmpasswordTextfields.text!.isEmpty
        default:
            return !emailTextfields.text!.isEmpty
        }
    }
    
    
    private func loginUser() {
        
        FireBaseUserlistener.shared.loginUserWithEmail(email: emailTextfields.text!, password: passwordTextfields.text!) { (error, userEmailIsVerified) in
            
            if error == nil {
                if userEmailIsVerified {
                    print("user has log in with email\(User.currentUser?.email)")
                    self.goToApp()

                } else {
                    ProgressHUD.showFailed("Please Verifi Email.")
                }
                
            }else {
                ProgressHUD.showFailed("\(error!.localizedDescription)")
            }
        }
        
        
    }
    
    
    
    
    private func registerUser() {
        
        if passwordTextfields.text == confirmpasswordTextfields.text {
            FireBaseUserlistener.shared.registerUserWith(emailTextfields.text!, passwordTextfields.text!) { (error) in
                if error == nil {
                    
                    
                    ProgressHUD.showSuccess("verification email sent.")
                    self.resendEmailBtn.isHidden = false
                } else {
                    ProgressHUD.showError(error!.localizedDescription)
                }
            }
        }else {
            ProgressHUD.showFailed("password's don't match")
        }
        
        
        
    }
    
    private func resetPassword() {
        
        FireBaseUserlistener.shared.resetPasswordFor(email: emailTextfields.text!) { (error) in
            
            if error == nil {
                
                ProgressHUD.showSuccess("reset password email sent")
            } else {
                ProgressHUD.showFailed(error!.localizedDescription)
            }
            
        }
        
        
    }
    
    private func resendVerificationEmail() {
        
        FireBaseUserlistener.shared.resendVerificationEmail(email: emailTextfields.text!) { (error) in
            
            if error == nil {
                ProgressHUD.showSuccess("verification email resend")
            } else {
                ProgressHUD.showFailed(error!.localizedDescription)
            }
            
        }
        
        
        
    }
     //MARK: - Navigation
    
    func goToApp()  {

        if   let mainVC = storyboard?.instantiateViewController(identifier: "MainVC") {
        mainVC.modalPresentationStyle = .fullScreen
        present(mainVC, animated: true, completion: nil)
        }
    }
    
    
}
