//
//  RecentTableViewCell.swift
//  Xapp
//
//  Created by Javad faghih on 10/10/1399 AP.
//

import UIKit

class RecentTableViewCell: UITableViewCell {

    
     //MARK: - IBActions
    
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var unreadCounterLabel: UILabel!
    @IBOutlet weak var lastMessagelbl: UILabel!
    @IBOutlet weak var unreadCounterView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.unreadCounterView.layer.cornerRadius = unreadCounterView.frame.size.width / 2
        
    }

    
    func configure(recentChat: RecentChat) {
        userNameLbl.text = recentChat.reciverName
        userNameLbl.adjustsFontSizeToFitWidth = true
        userNameLbl.minimumScaleFactor = 0.9
        
        lastMessagelbl.text = recentChat.lastMessage
        lastMessagelbl.numberOfLines = 2
        lastMessagelbl.adjustsFontSizeToFitWidth = true
        userNameLbl.minimumScaleFactor = 9.0
        

        
        if recentChat.underCounter != 0 {
            unreadCounterLabel.text = "\(recentChat.underCounter)"
            unreadCounterLabel.isHidden = false
            unreadCounterView.isHidden = false
            
        } else {
            
            unreadCounterView.isHidden = true
        }
        
        
        setAvatar(avatarLink: recentChat.avatarLink)
        dateLbl.text = timeElapsed(recentChat.date ?? Date())
    }
    
    private func setAvatar(avatarLink: String) {
        
        if avatarLink != "" {
        FileStorage.downloadImage(imageUrl: avatarLink) { (avatarImage) in
            
          
            DispatchQueue.main.async {
                self.avatarImageView.image = avatarImage?.circleMasked
            
            }
                }
            }
        else {
                self.avatarImageView.image = UIImage(named: "avatar")
                
            }
        
        }

}
