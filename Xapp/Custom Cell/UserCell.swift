//
//  UserCell.swift
//  Xapp
//
//  Created by Javad faghih on 10/9/1399 AP.
//

import UIKit

class UserCell: UITableViewCell {

     //MARK: - IBOutlets
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(user: User) {
        
        nameLbl.text = user.username
        statusLabel.text = user.status
        
        setavatar(avatarLink: user.avatarLink)
        
    }
    
    private func setavatar(avatarLink: String) {
        
        if avatarLink != "" {
            FileStorage.downloadImage(imageUrl: avatarLink) { (avatarImage) in
           
                DispatchQueue.main.async {
                    self.avatarImageView.image = avatarImage?.circleMasked

                }
            }
        }
        else {
            
            self.avatarImageView.image = UIImage(named: "avatar")
        }
    }
    

}
