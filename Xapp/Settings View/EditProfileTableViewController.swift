//
//  EditProfileTableViewController.swift
//  Xapp
//
//  Created by Javad faghih on 10/6/1399 AP.
//

import UIKit
import Gallery
import ProgressHUD
class EditProfileTableViewController: UITableViewController {

     //MARK: - IBOutlets
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var usernameTextField: UITextField!
    
    
     //MARK: - Vars
    var gallery: GalleryController!
    
    
     //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.tableFooterView = UIView()
        configureTextField()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    
        showUserInfo()
        
    }
    
     //MARK: - IBActions
    
    @IBAction func editImageProfileButtonPressed(_ sender: UIButton) {

        showImageGallery()
        
    }
    
     //MARK: - TableView Delegate
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0.0 : 30
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
      
        let header = UIView()
        header.backgroundColor = UIColor(named: "tableviewBackgroundColor")
        
        return header
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        // to show status view
        
        if indexPath.section == 1 && indexPath.row == 0 {
            
            performSegue(withIdentifier: "editProfileToStatusSegue", sender: self)
        }
        
    }
    
     //MARK: - UPDate UI
    private func showUserInfo() {
        
        if let user = User.currentUser {
            
            statusLabel.text = user.status
        
            if user.avatarLink != "" {
              // set avatar image
    

                FileStorage.downloadImage(imageUrl: user.avatarLink) { (avatarImage) in
                    self.avatarImageView.image = avatarImage?.circleMasked
                }
                
            }
        }
    }

    
    
    
     //MARK: - Configure
    
    private func configureTextField() {
        
        usernameTextField.delegate = self
        
    }
    
    //MARK: - Gallery
   
   private func showImageGallery() {
   
    self.gallery = GalleryController()
    self.gallery.delegate = self
       
    Config.tabsToShow = [.cameraTab, .imageTab]
    Config.Camera.imageLimit = 1
    Config.initialTab = .imageTab
    
    
    self.present(gallery, animated: true, completion: nil)
   
   }
    
    
    
     //MARK: - Upload Images
    
    private func uploadAvatarImage(_ image: UIImage) {
    
        let fileDirectory = "Avatars/" + "_\(User.currentID)" + ".jpg"
        
        FileStorage.uploadImage(image, directory: fileDirectory) { (avatarLink) in
            
            if var user = User.currentUser {
                user.avatarLink = avatarLink ?? ""
                saveUserlocaly(user: user)
                FireBaseUserlistener.shared.saveUserToFirestore(user)
                
                
            }
            
            //TODO: save Image localy
            FileStorage.saveFileLocally(fileData: image.jpegData(compressionQuality: 1.0)! as NSData, fileName: User.currentID)
            
            
        }
    }
    
    
}

extension EditProfileTableViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == usernameTextField {
            
            if !textField.text!.isEmpty {
                
                if var user = User.currentUser {
                    
                    user.username = textField.text!
                    saveUserlocaly(user: user)
                    FireBaseUserlistener.shared.saveUserToFirestore(user)
                }
            }
            textField.resignFirstResponder()
            return false
        }
        return true
    }
 
}


extension EditProfileTableViewController: GalleryControllerDelegate {
   
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
     
        if images.count > 0 {
            
            images.first!.resolve { (avatarImage) in
                if avatarImage != nil {
              
                    self.uploadAvatarImage(avatarImage!)
                    self.avatarImageView.image = avatarImage?.circleMasked
                    //TODO: upload image
                
                } else {
                    
                    ProgressHUD.showFailed("couldn't find any photo")
                }
            }
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
}


