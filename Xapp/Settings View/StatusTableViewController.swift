//
//  StatusTableViewController.swift
//  Xapp
//
//  Created by Javad faghih on 10/9/1399 AP.
//

import UIKit

class StatusTableViewController: UITableViewController {

     //MARK: - Vars
    
    var allStatus: [String] = []
    
    
     //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
       tableView.tableFooterView = UIView()
        loadUserStatus()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allStatus.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell = tableView.dequeueReusableCell(withIdentifier: "statusCell", for: indexPath)
        var status = allStatus[indexPath.row]
        cell.textLabel!.text = status
        
        cell.accessoryType = User.currentUser?.status == status ? .checkmark : .none
        return cell
    }
    
   
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        updateCellCheck(indexPath)
        tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var header = UIView()
        header.backgroundColor = UIColor(named: "tableviewBackgroundColor")
        
        return header
    }

    
         //MARK: - LoadingStatus
   private func loadUserStatus() {
        
    allStatus = userDefaults.object(forKey: kSTATUS) as! [String]
        
    tableView.reloadData()
        
        
    }
    
    private func updateCellCheck(_ indexPath: IndexPath) {
        
        if var user = User.currentUser {
            user.status = allStatus[indexPath.row]
            saveUserlocaly(user: user)
            FireBaseUserlistener.shared.saveUserToFirestore(user)
            
        }
        
        
        
    }
    
    
}
