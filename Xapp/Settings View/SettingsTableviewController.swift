//
//  SettingsTableviewController.swift
//  Xapp
//
//  Created by Javad faghih on 10/3/1399 AP.
//

import UIKit
import ProgressHUD

class SettingsTableviewController: UITableViewController {

     //MARK: - IBOutlets
    
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var appVersionLabel: UILabel!
    
    
     //MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.tableFooterView = UIView()
        showUserInfo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        showUserInfo()
    }
    
     //MARK: - Table View Delegates
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return section == 0 ? 0.0 : 10.0
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        tableView.deselectRow(at: indexPath, animated: true)
   
        if indexPath.section == 0 && indexPath.row == 0 {
            
            performSegue(withIdentifier: "settingToEditProfileSeg", sender: self)
            
        }
    }
    
    
    

     //MARK: - IBActions
    
    @IBAction func tallAFriendButtonPressed(_ sender: UIButton) {
 
    print("tell a freiend")
        
    }
    
    @IBAction func termsAndConditionButtonPressed(_ sender: UIButton) {
    
    print("terms and conditions")
        
    }
    
    @IBAction func logOutButtonPressed(_ sender: UIButton) {
  
        FireBaseUserlistener.shared.logOutCurrentUser { (error) in
            
            if error == nil {
              
                if let loginVC = self.storyboard?.instantiateViewController(identifier: "loginVC") {
                    loginVC.modalPresentationStyle = .fullScreen
                    
                    DispatchQueue.main.async {
                        self.present(loginVC, animated: true, completion: nil)
                    }
                    
                }
                
                
                
            } else {
                ProgressHUD.showFailed(error?.localizedDescription)
            }
            
        }
        
    }
    
     //MARK: - UpDate UI
    
    private func showUserInfo() {
        
        
        if let user = User.currentUser {
            
            usernameLabel.text = user.username
            statusLabel.text = user.status
            appVersionLabel.text = "app version \(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "")"
            
            if user.avatarLink != "" {
                // download and set Avatar Image
                
                
                FileStorage.downloadImage(imageUrl: user.avatarLink) { (avatarImage) in
                    self.avatarImageView.image = avatarImage?.circleMasked
                }
                
            }
            
        }
        
    }
    
}
