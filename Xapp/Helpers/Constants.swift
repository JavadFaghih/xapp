//
//  Constants.swift
//  Xapp
//
//  Created by Javad faghih on 9/30/1399 AP.
//

import Foundation


let userDefaults = UserDefaults.standard
public let kFILEREFERENCE = "gs://xapp-214f7.appspot.com"

public let kSERVERKEY  =  "AAAA0ZPP7R8:APA91bG4MmUeUFKWPwZlbz4StZUnha2UAUWAZDJJyInK36IVq0hJo-V94Yr4kyia8xcLeIwmhVBzn0leNT2iiv5KfiIAo3nudSJrzeMPa-5IIH80ZLHBvBvzDxin0MiAC3KDHkoJMFxb"

public let kNUMBERMESSAGES = 12
 
public let kCURRENTUSER = "currentuser"
public let kSTATUS = "userstatus"
public let kFIRSTRUN = "FirstRun"

public let kCHATROOMID = "chatRoomId"
public let kSENDERID = "senderId"

public let kSENT = "sent"
public let kREAD = "read"

public let kTEXT = "text"
public let kPHOTO = "photo"
public let kVIDEO = "video"
public let kAUDIO = "audio"
public let kLOCATION = "location"

public let kDATE = "date"
public let kREADDATE = "date"

public let kADMINID = "adminId"
public let kMEMBERIDS = "memberIds"
