//
//  GlobalFunctions.swift
//  Xapp
//
//  Created by Javad faghih on 10/8/1399 AP.
//

import AVFoundation
import UIKit

func removeCurrentUserFrom(userIds: [String]) -> [String] {
    
    var allIds = userIds
    
    for id in allIds {
        
        if id == User.currentID {
            allIds.remove(at: allIds.firstIndex(of: id)!)
        }
    }
    return allIds
}


func fileNamefrom(fileURL: String) -> String {
    
    let name = ((fileURL.components(separatedBy: "_").last)!.components(separatedBy: "?").first!).components(separatedBy: ".").first!
    
    return name
    
}



func timeElapsed(_ date: Date) -> String {
    
    let seconds = Date().timeIntervalSince(date)
    
    var elapsed = ""
    if seconds < 60.0 {
        elapsed = "just now"
    } else if seconds < 60 * 60 {
        
        let minutes = Int(seconds / 60)
        
        let minText = minutes > 1 ? "mins" : "min"
        
        elapsed = "\(minutes) \(minText)"
        
    } else if seconds < 24 * 60 * 60 {
        
        
        let hours = Int(seconds / (60 * 60))
        let hourText = hours > 1 ? "hours" : "hour"
        
        elapsed = "\(hours) \(hourText)"
    } else {
        
        elapsed = date.longDate() 
    }
    
    
    
    return elapsed
}

func videoThumbnail(video: URL) -> UIImage {
    
   var asset = AVURLAsset(url: video, options: nil)
    
    let imagegeneretaor = AVAssetImageGenerator(asset: asset)
    imagegeneretaor.appliesPreferredTrackTransform = true
    
    let time = CMTimeMakeWithSeconds(0.5, preferredTimescale: 1000)
    var actualTime = CMTime.zero
    var image: CGImage?
    
    do {
        image = try imagegeneretaor.copyCGImage(at: time, actualTime: &actualTime)
    }
    catch let error as NSError {
        print("error making thumbnail", error.localizedDescription)
    }
    
    if image != nil {
        return UIImage(cgImage: image!)

    } else {
        return UIImage(named: "photoPlaceholder")!
    }
}



