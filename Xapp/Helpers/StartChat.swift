//
//  StartChat.swift
//  Xapp
//
//  Created by Javad faghih on 10/13/1399 AP.
//

import Foundation
import Firebase


 //MARK: - StartChat
func startChat(user1: User, user2: User) -> String {
    
    let chatRoomId = chatRoomIdFrom(userId1: user1.id, userId2: user2.id)
    
    createRecentItems(chatRoomId: chatRoomId, users: [user1, user2])
    
    return chatRoomId
}

func restartChat(chatRoomId: String, memberIds: [String]) {

    FireBaseUserlistener.shared.downloadUsersFromFirebase(withIds: memberIds) { (users) in
        
        
        if users.count > 0 {
            
            createRecentItems(chatRoomId: chatRoomId, users: users)
            
        }
        
        
    }
    
}


func createRecentItems(chatRoomId: String, users: [User]) {
  
    var memberIdsToCreateRecent = [users.first!.id, users.last!.id]
    print("members to create recent is ...", memberIdsToCreateRecent)
    //does user have recent?
    firebaseReference(.Recent).whereField(chatRoomId, isEqualTo: chatRoomId).getDocuments { (snapShot, error) in
        
        guard let snapShot = snapShot else { return }
        
        if !snapShot.isEmpty {
            memberIdsToCreateRecent = removeMemberWhoHasRecent(snapshot: snapShot, memberIds: memberIdsToCreateRecent)
            print("updated members to create recent is ...", memberIdsToCreateRecent)

        }
        for userId in memberIdsToCreateRecent {
            
            
            print("creating recent for user with id ", userId)
            let senderUser = userId == User.currentID ? User.currentUser! : getReceiverFrom(users: users)
            
            
            let receiverUser = userId == User.currentID  ? getReceiverFrom(users: users) : User.currentUser!
            
            let recentObject = RecentChat(id: UUID().uuidString, chatRoomId: chatRoomId, senderId: senderUser.id, senderName: senderUser.username, reciverId: receiverUser.id, reciverName: receiverUser.username, date: Date(), memberIds: [senderUser.id, receiverUser.id], lastMessage: "", underCounter: 0, avatarLink: receiverUser.avatarLink)
            
            FirebaseRecentListener.shared.saveRecent(recentObject)
            
            
            
        }
        
        
    }
}

func removeMemberWhoHasRecent(snapshot: QuerySnapshot, memberIds: [String]) -> [String] {
    
    var memberIdsToCreateRecent = memberIds
    
    for recentData in snapshot.documents {
        
        let currentRecent = recentData.data() as Dictionary
        
        if let currentUserId = currentRecent[kSENDERID] {
            
            if memberIdsToCreateRecent.contains(currentUserId as! String) {
                
                
                memberIdsToCreateRecent.remove(at: memberIdsToCreateRecent.firstIndex(of: currentUserId as! String)!)
            }
            
        }
        
    }
    return memberIdsToCreateRecent
    
}



func chatRoomIdFrom(userId1: String, userId2: String) -> String {
    
    var chatRoomId = ""
     
  let  value = userId1.compare(userId2).rawValue
    
    chatRoomId = value < 0 ? (userId1 + userId2) : (userId2 + userId1)
    
    return chatRoomId
}


func getReceiverFrom(users: [User]) -> User {
    
    var allUsers = users
    
    
    allUsers.remove(at: allUsers.firstIndex(of: User.currentUser!)!)
    return allUsers.first!
}
