//
//  UserVC.swift
//  Xapp
//
//  Created by Javad faghih on 10/9/1399 AP.
//

import UIKit

class UserVC: UIViewController ,UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate {
  

     //MARK: - Vars
    
    var allusers: [User] = []
    var filteredUser: [User] = []

    
    let searchController = UISearchController(searchResultsController: nil)
    
    @IBOutlet weak var tableView: UITableView!
    
     //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.tableView.refreshControl = UIRefreshControl()
        self.tableView.refreshControl = self.tableView.refreshControl
        
       //createDummyUser()
      
        setupSearchController()
        
        downloadUser()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.largeTitleDisplayMode = .always
        navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    
     //MARK: - TableView Delegate
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchController.isActive ? filteredUser.count : allusers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
   
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: "userCell", for: indexPath) as? UserCell else { return UITableViewCell() }
        
        let user = searchController.isActive ? filteredUser[indexPath.row] : allusers[indexPath.row]
        
        cell.configureCell(user: user)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footer = UIView()
        footer.backgroundColor = UIColor(named: "tableviewBackgroundColor")
        return footer
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        var user = searchController.isActive ? filteredUser : allusers
        
        if let profileVC = storyboard?.instantiateViewController(identifier: "ProfileVC") as? ProfileTableViewController {
            
            profileVC.modalPresentationStyle = .fullScreen
            
            profileVC.user = user[indexPath.row]
            
          //  navigationController?.pushViewController
            self.navigationController?.pushViewController(profileVC, animated: true)
            
            
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor(named: "tableviewBackgroundColor")
        return header
    }
    
    
    
     //MARK: - Download Users
    private func downloadUser() {
        
        FireBaseUserlistener.shared.downloadAllUsersFromFirebase { (allfirebaesUser) in
            self.allusers = allfirebaesUser
            print(allfirebaesUser)
            
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
        
        
        
        
    }

    
     //MARK: - Setup Search
    
    private func setupSearchController() {
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = true
        
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "search user"
        searchController.searchResultsUpdater = self
        definesPresentationContext = true
    }
    
    private func filteredContentForSearchText(searchText: String) {
        
        filteredUser = allusers.filter({ (user) -> Bool in
            return user.username.lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()
    }
    
     //MARK: - UIScrollViewDelegate
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        print("rfresh")
        if self.tableView.refreshControl!.isRefreshing {
            self.downloadUser()
            self.tableView.refreshControl!.endRefreshing()
        }
        
    }
    
    
 
    
}


extension UserVC: UISearchResultsUpdating {
  
    func updateSearchResults(for searchController: UISearchController) {
        
        filteredContentForSearchText(searchText: searchController.searchBar.text!)
        
    }
    
     //MARK: - Navigation
    
    private func showUserProfile(_ user: User) {
        
        
        
        
    }
    
}
