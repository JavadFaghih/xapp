//
//  MapViewController.swift
//  Xapp
//
//  Created by Javad faghih on 10/26/1399 AP.
//

import UIKit
import CoreLocation
import MapKit

class MapViewController: UIViewController {

     //MARK: - Vars
    var location: CLLocation?
    var mapView: MKMapView!
    
    
    
     //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        configuretitle()
        configureMapView()
        configureLeftBarButton()
        
    }
     //MARK: - Configurations
    private func configureMapView() {
        mapView = MKMapView(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        mapView.showsUserLocation = true
        
        if location != nil {
            mapView.setCenter(location!.coordinate, animated: true)
            //add annotation
            mapView.addAnnotation(Mapannotation(title: nil, coordinate: location!.coordinate))
            
            
            
            
            
        }
        view.addSubview(mapView)
        
    }
    
    private func configureLeftBarButton() {
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "chevron.left"), style: .plain, target: self, action: #selector(self.backbuttonPressed))
        
        
    }
  
    private func configuretitle() {
        
        self.title = "Map View"
        
    }

     //MARK: - actions
    @objc func backbuttonPressed() {
        self.navigationController?.popViewController(animated: true)
        
        
    }
}
