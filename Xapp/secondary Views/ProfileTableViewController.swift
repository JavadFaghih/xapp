//
//  ProfileTableViewController.swift
//  Xapp
//
//  Created by Javad faghih on 10/10/1399 AP.
//

import UIKit

class ProfileTableViewController: UITableViewController {

    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    
    var user: User?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.largeTitleDisplayMode = .never
        
        
        
        self.tableView.tableFooterView = UIView()
       confirmeCell()
    }

    // MARK: - Table view delegates

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = UIView()
        header.backgroundColor = UIColor(named: "tableviewBackgroundColor")
        return header
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        
        tableView.deselectRow(at: indexPath, animated: true)
  
        if indexPath.section == 0 && indexPath.row == 1 {

            let chatId = startChat(user1: User.currentUser!, user2: user!)
            
            let privateChatViewController = ChatViewController(chatId: chatId, recipientId: user!.id, recipientName: user!.username)
            
            
            
            navigationController?.hidesBottomBarWhenPushed = true
            navigationController?.pushViewController(privateChatViewController, animated: true)
        }
    
    
    
    }
    
    private func confirmeCell() {
        
        if user != nil {
            userNameLbl.text = user!.username
            statusLabel.text = user!.status
            title = user!.username
       //     avatarImageView.image = 
          
            if user?.avatarLink != nil {
                
                FileStorage.downloadImage(imageUrl: user!.avatarLink) { (avatarImage) in
             
                    DispatchQueue.main.async {
                        self.avatarImageView.image = avatarImage?.circleMasked

                    }
                }
            }
            
        }
    }
}
