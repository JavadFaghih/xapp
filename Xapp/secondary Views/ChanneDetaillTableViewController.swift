//
//  ChannelTableViewController.swift
//  Xapp
//
//  Created by Javad faghih on 10/28/1399 AP.
//

import UIKit

protocol ChannelDetailTableViewControllerDelegate {
    func didclickFollow()
}

class ChannelTableViewController: UITableViewController {

     //MARK: - IBOutlets
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var memberLabel: UILabel!
    @IBOutlet weak var aboutTextView: UITextView!
    
     //MARK: - Vars
    var channel: Channel!
    var delegate: ChannelDetailTableViewControllerDelegate?
    
    
 //MARK: - View LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.largeTitleDisplayMode = .never
        tableView.tableFooterView = UIView()
   showChannelData()
        configureRightBarButton()
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return UITableViewCell()
    }
    
     //MARK: - Configure
    private func showChannelData() {
        self.title = channel.name
        nameLabel.text = channel.name
        memberLabel.text = "\(channel.memberIds.count)"
        aboutTextView.text = channel.aboutChannel
        setAvatar(avatarLink: channel.avatarLink)
    }
    
    private func configureRightBarButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Follow", style: .plain, target: self, action: #selector(followChannel))
    }
    
    private func setAvatar(avatarLink: String) {
        print("hello")
        if avatarLink != "" {
            FileStorage.downloadImage(imageUrl: avatarLink) { (avatarImage) in
            
                self.avatarImageView.image = avatarImage != nil ? avatarImage?.circleMasked : UIImage(named: "avatar")
            }
            
        } else {
            
            self.avatarImageView.image = UIImage(named: "avatar")
        }
    }
    
     //MARK: - Actions
    @objc func followChannel() {
        
        channel.memberIds.append(User.currentID)
        FirebaseChannelListener.shared.saveChannel(channel)
        delegate?.didclickFollow()
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
