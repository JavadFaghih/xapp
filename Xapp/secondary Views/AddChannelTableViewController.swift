//
//  AddChannelTableViewController.swift
//  Xapp
//
//  Created by Javad faghih on 10/27/1399 AP.
//

import UIKit
import Gallery
import ProgressHUD

class AddChannelTableViewController: UITableViewController {
    
     //MARK: - IBOutlets
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var aboutTextView: UITextView!
    
     //MARK: - Vars
    var gallery: GalleryController!
    var tapGesture = UITapGestureRecognizer()
    var avatarLink = ""
    var channelId = UUID().uuidString
    
    var channelToEdit: Channel?
    
     //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.largeTitleDisplayMode = .never
        tableView.tableFooterView = UIView()
        
        configureGesture()
        configureLeftBarbutton()
        if channelToEdit != nil {
            
            configureToEditingView()
            
            
            
        }
    }

    // MARK: - Table view data source

 

     //MARK: - IBActions
    
    @IBAction func svaeButtonPressed(_ sender: Any) {
        if !nameTextField.text!.isEmpty {
            if channelToEdit != nil {
                editChannel()
                //edit
            } else {
               //save new
                saveChannel()

            }
            
        } else {
            ProgressHUD.showError("channel name is empty")
        }
        
        
        
    }
    
    
    
    
     //MARK: - Configuration
    private func configureGesture() {
        tapGesture.addTarget(self, action: #selector(avatarImageTap))
        avatarImageView.isUserInteractionEnabled = true
        avatarImageView.addGestureRecognizer(tapGesture)
    }
    @objc func avatarImageTap() {

        showGallery()
        
        
        
    }
    
    private func configureLeftBarbutton() {
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "chevron.left"), style: .plain, target: self, action: #selector(backButtonPressed))
        
        
    }
    
    private func configureToEditingView() {
        self.nameTextField.text = channelToEdit!.name
        self.channelId = channelToEdit!.id
        self.aboutTextView.text = channelToEdit!.aboutChannel
        self.avatarLink = channelToEdit!.avatarLink
        self.title = "Editing Channel"
        
        
        setAvatar(avatarLink: channelToEdit!.avatarLink)
        
    }
    
   
    
    @objc func backButtonPressed() {
        
        navigationController?.popViewController(animated: true)
        
        
    }
     //MARK: - Save Channel
    private func saveChannel() {
        
        let channel = Channel(id: channelId, name: nameTextField.text!, adminId: User.currentID, memberIds: [User.currentID], avatarLink: avatarLink, aboutChannel: aboutTextView.text)
        
        FirebaseChannelListener.shared.saveChannel(channel)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    private func editChannel() {
        
        channelToEdit!.name = nameTextField.text!
        channelToEdit!.aboutChannel = aboutTextView.text
        channelToEdit!.avatarLink = avatarLink
        
        FirebaseChannelListener.shared.saveChannel(channelToEdit!)
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
     //MARK: - Gallery
    private func showGallery()  {
        self.gallery = GalleryController()
        gallery?.delegate = self
        Config.tabsToShow = [.imageTab, .cameraTab]
        Config.Camera.imageLimit = 1
        Config.initialTab = .imageTab
        
        self.present(gallery, animated: true, completion: nil)
        
    }
    
     //MARK: - Avatars
    private func uploadAvatarImage(_ image: UIImage) {
        
        let fileDirectory = "Avatars/" + "_\(channelId)" + ".jpg"
        
        FileStorage.saveFileLocally(fileData: image.jpegData(compressionQuality: 1.0)! as NSData, fileName: channelId)
        
        FileStorage.uploadImage(image, directory: fileDirectory) { (avatarLink) in
            
            self.avatarLink = avatarLink ?? ""
            
            
        }
        
    }
    
    private func setAvatar(avatarLink: String) {
        
        if avatarLink != "" {
            
            FileStorage.downloadImage(imageUrl: avatarLink) { (avatarImage) in
                
                DispatchQueue.main.async {
                    self.avatarImageView.image = avatarImage?.circleMasked
                }
                
            }
            } else {
                self.avatarImageView.image = UIImage(named: "avatar")
                
            }
            
        }
        
        
    }
extension AddChannelTableViewController: GalleryControllerDelegate {
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
    
        if images.count > 0 {
            
            images.first!.resolve { (icon) in
                if icon != nil {
                    
                    self.uploadAvatarImage(icon!)
                    self.avatarImageView.image = icon!.circleMasked
                } else {
                    ProgressHUD.showFailed("Couldnt select image")
                }
            }
            
        }
        
        controller.dismiss(animated: true, completion: nil)
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    
    
    
    
    
    
    
}
