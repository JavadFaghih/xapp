//
//  ChatViewController.swift
//  Xapp
//
//  Created by Javad faghih on 10/16/1399 AP.
//

import UIKit
import MessageKit
import RealmSwift
import InputBarAccessoryView
import Gallery


class ChatViewController: MessagesViewController {

     //MARK: - Views
    let leftbarButtonView: UIView = {
        return UIView(frame: CGRect(x: 0, y: 0, width: 200, height: 50))
    }()
    
    let titleLabel: UILabel = {
        let title = UILabel(frame: CGRect(x: 5, y: 0, width: 180, height: 25))
        title.textAlignment = .left
        title.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        title.adjustsFontSizeToFitWidth = true
        
        return title
    }()
    
    
    let subtitleLabel: UILabel = {
        let subTitle = UILabel(frame: CGRect(x: 5, y: 22, width: 180, height: 25))
        subTitle.textAlignment = .left
        subTitle.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        subTitle.adjustsFontSizeToFitWidth = true
        
        return subTitle
    }()
    
    
     //MARK: - Vars
    private var chatId = ""
    private var recipientId = ""
    private var recipientName = ""
    
    open lazy var audioController = BasicAudioController(messageCollectionView: messagesCollectionView)
    
    let currentUser = MKSender(senderId: User.currentID, displayName: User.currentUser!.username)
    
    
    let refreshController = UIRefreshControl()
  
    let micButton = InputBarButtonItem()
    
    var mkMessages:[MKMessage] = []
    
    let realm = try! Realm()
    
    var allLocalMessage: Results<LocalMessage>!
    
    var displayingMessagesCount = 0
    var maxMessageNumber = 0
    var minMessageNumber = 0
    var typingCounter = 0
    
    var gallery: GalleryController!
    
    
    
    //Listeners
    var notificationToken: NotificationToken?
    
    var longPressGesture: UILongPressGestureRecognizer!
    var audioFileName = ""
    var audioDuration: Date!
    
    
    
    
     //MARK: - inits
    init(chatId: String, recipientId: String, recipientName: String) {
        
        super.init(nibName: nil, bundle: nil)
        
        
        self.chatId = chatId
        self.recipientId = recipientId
        self.recipientName = recipientName
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)

    }
    
     //MARK: - ViewLifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.largeTitleDisplayMode = .never
        
        createTypingObserver()
    
        configureMessageCollectionView()
        configureGesturerecognizer()
        configureMessageInputBar()
     
        configurationLeftBarButton()
        configureCustomTitle()

        
        loadChats()
        listenForNewChats()
        updateMicButtonStatus(show: true)
        listenForReadstatusChange()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
  
        FirebaseRecentListener.shared.resetRecentCounter(chatRoomId: chatId)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
   
        FirebaseRecentListener.shared.resetRecentCounter(chatRoomId: chatId)
        audioController.stopAnyOngoingPlaying()
    }
    
    
    
     //MARK: - Configuration
    private func configureMessageCollectionView() {
    
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messageCellDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messagesCollectionView.messagesLayoutDelegate = self
        
        messagesCollectionView.refreshControl = refreshController
        
        scrollsToBottomOnKeyboardBeginsEditing = true
        maintainPositionOnKeyboardFrameChanged = true
    }
    
    private func configureGesturerecognizer() {
        longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(recordAudio))
        
        longPressGesture.minimumPressDuration = 0.5
        longPressGesture.delaysTouchesBegan = true
    }
    
    
    
    private func configureMessageInputBar() {
        
        messageInputBar.delegate = self
        
        let attachButton = InputBarButtonItem()
        
        
        attachButton.image = UIImage(systemName: "plus", withConfiguration: UIImage.SymbolConfiguration(pointSize: 25))
        
        attachButton.setSize(CGSize(width: 30, height: 30), animated: true)
        
        attachButton.onTouchUpInside {
            item in
            

            self.actionAttachMessage()

        }
        
        micButton.image = UIImage(systemName: "mic.fill", withConfiguration: UIImage.SymbolConfiguration(pointSize: 25))
        micButton.setSize(CGSize(width: 30, height: 30), animated: true)
        
        micButton.addGestureRecognizer(longPressGesture)
        
        messageInputBar.setStackViewItems([attachButton], forStack: .left, animated: true)
  
        messageInputBar.setLeftStackViewWidthConstant(to: 36, animated: true)
            
        messageInputBar.inputTextView.isImagePasteEnabled = false
        messageInputBar.backgroundView.backgroundColor = .systemBackground
        messageInputBar.inputTextView.backgroundColor = .systemBackground
        
        
    }
    
    func updateMicButtonStatus(show: Bool) {
        
        if show {
            messageInputBar.setStackViewItems([micButton], forStack: .right, animated: true)
            messageInputBar.setRightStackViewWidthConstant(to: 30, animated: true)
        } else {
            
            messageInputBar.setStackViewItems([messageInputBar.sendButton], forStack: .right, animated: true)
            messageInputBar.setRightStackViewWidthConstant(to: 55, animated: true)
        }
        
        
        
    }
    
    
    
    
    
    
    
    
    
    private func configurationLeftBarButton() {
        
        self.navigationItem.leftBarButtonItems = [UIBarButtonItem(image: UIImage(systemName: "chevron.left"), style: .plain, target: self, action: #selector(self.backButtonPressed))]
    }
    
    
    private func configureCustomTitle() {
        
        leftbarButtonView.addSubview(titleLabel)
        leftbarButtonView.addSubview(subtitleLabel)
        
        let leftBarButtonItem = UIBarButtonItem(customView: leftbarButtonView)

        self.navigationItem.leftBarButtonItems?.append(leftBarButtonItem)
        titleLabel.text = recipientName
    }
    
   
  //MARK: - Load Chats
    private func loadChats() {
    
  let  predicate = NSPredicate(format: "chatRoomId = %@", chatId)
    
        allLocalMessage = realm.objects(LocalMessage.self).filter(predicate).sorted(byKeyPath: kDATE, ascending: true)
        
        if allLocalMessage.isEmpty {
            
            checkForOldChats()
        }
        
        
        
        
        notificationToken = allLocalMessage.observe({ (changes: RealmCollectionChange) in
            
            switch changes {
            
            case .initial(_):
                self.insertMessages()
                self.messagesCollectionView.reloadData()
                self.messagesCollectionView.scrollToBottom(animated: true)
                print("we have \(self.allLocalMessage.count)")
            case .update(_,_,let insertions, _):
                
                for index in insertions {
                    
                    
                    self.insertMessage(self.allLocalMessage[index])
                    self.messagesCollectionView.reloadData()

                    self.messagesCollectionView.scrollToBottom(animated: false)
                }
                
            case .error(let error):
                print("Error on new insertion \(error.localizedDescription)")
            }
            
            
        })

    }
    
    private func listenForNewChats() {
        
        FirebaseMessageListener.shared.listenForNewChats(User.currentID, collectionId: chatId, lastMessageDate: lastMessageDate())
        
        
    }
    
    private func checkForOldChats() {
        
        FirebaseMessageListener.shared.checkForOldChats(User.currentID, collectionId: chatId)
        
    }
    

     //MARK: - insert Messages
    private func listenForReadstatusChange() {
        
        FirebaseMessageListener.shared.listenForReadStatusChanges(User.currentID, collectionId: chatId) { (updatedMessage) in
         
            if updatedMessage.status != kSENT {

            
            self.updatedMessage(updatedMessage)
            }
        }
        
        
        
    }

    
    
    
    private func insertMessages() {
        
  
        maxMessageNumber = allLocalMessage.count - displayingMessagesCount
        minMessageNumber = maxMessageNumber - kNUMBERMESSAGES
        
        if minMessageNumber < 0 {
            minMessageNumber = 0
        }
        
        for i in minMessageNumber ..< maxMessageNumber {
            insertMessage(allLocalMessage[i])
        }
        
        
    }
    
  private  func insertMessage(_ localMessage: LocalMessage) {

    if localMessage.senderId != User.currentID {
        markMessageAsRead(localMessage)

    }
    
    
    let incoming = IncomingMessage(_collectionView: self)
        self.mkMessages.append(incoming.createMessage(localMesaage: localMessage)!)
        
        
        displayingMessagesCount += 1
        
    }
    
    private func loadMoreMessages(maxNember: Int, minNumber: Int) {
        
        maxMessageNumber = minNumber - 1
        minMessageNumber = maxMessageNumber - kNUMBERMESSAGES
        
        if minNumber < 0 {
            minMessageNumber = 0
        }
        
        for i in (minMessageNumber ... maxMessageNumber).reversed() {

insertOlderMessage(allLocalMessage[i])

        }
        
        
    }
    
    private  func insertOlderMessage(_ localMessage: LocalMessage) {
          print("inserted message")
          let incoming = IncomingMessage(_collectionView: self)
        self.mkMessages.insert(incoming.createMessage(localMesaage: localMessage)!, at: 0)
            
          
          displayingMessagesCount += 1
          
      }
    private func markMessageAsRead(_ localMessage: LocalMessage) {
        
        if localMessage.senderId != User.currentID && localMessage.status != kREAD {
            
            FirebaseMessageListener.shared.updateMessageInFirebase(localMessage, memberIds: [User.currentID, recipientId])
            
        }
        
        
    }
    
    
    
    
    
    
     //MARK: - Actions
    
    func messageSend(text: String?, photo: UIImage?, video: Video?, adio: String?, location: String?, audioDuration: Float = 0.0 ) {
        
        print("sending text", text)
        
        
        OutgoingMessage.send(chatId: chatId, text: text, photo: photo, video: video, audio: adio, audioDuration: audioDuration ,location: location, memberIds: [User.currentID, recipientId])
        
    }
    
    @objc func backButtonPressed() {
        //TODO:  remove listeners
        
        FirebaseRecentListener.shared.resetRecentCounter(chatRoomId: chatId)
        
        
        removeListener()
        self.navigationController?.popViewController(animated: true)
    }
    
    
    private func actionAttachMessage() {
    
        messageInputBar.inputTextView.resignFirstResponder()
        
        let optionMenu = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
       
        
        let takePhotoOrVideo = UIAlertAction(title: "Camera", style: .default) { (alert) in

            self.showImageGallery(camera: true)

        }
        
        let sharedMedia = UIAlertAction(title: "Library", style: .default) { (alert) in

            self.showImageGallery(camera: false)
        }
        
        let sharedlocation = UIAlertAction(title: "Share Location", style: .default) { (alert) in

            if let _ = LocationManager.shared.currentLocation {
                self.messageSend(text: nil, photo: nil, video: nil, adio: nil, location: kLOCATION)
            } else {
                
                print("no access to location")
            }



        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
       
        takePhotoOrVideo.setValue(UIImage(systemName: "camera"), forKey: "image")
        sharedMedia.setValue(UIImage(systemName: "photo.fill"), forKey: "image")
        sharedlocation.setValue(UIImage(systemName: "mappin.and.ellipse"), forKey: "image")

        
        optionMenu.addAction(takePhotoOrVideo)
        optionMenu.addAction(sharedlocation)
        optionMenu.addAction(sharedMedia)
        optionMenu.addAction(cancelAction)
        present(optionMenu, animated: true, completion: nil)


    }
    
    
     //MARK: - Up date Typing indicator
   
    func createTypingObserver() {
        
        FireBaseTypingListener.shared.cratetypingobserver(chatRoomId: chatId) { (isTyping) in
            DispatchQueue.main.async {
                self.updateTypingIndicator(isTyping)
            }
        }
        
        
    }
    
    
    
    
    
    
    
    func typingIndicatorUpdate() {
        
        typingCounter += 1
        print("test.........")
        FireBaseTypingListener.saveTyping(typing: true, chatRoomId: chatId)
   
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            //stop typing
            self.typingCounterStop()
        }
    }
    
    func typingCounterStop() {
        typingCounter -= 1
        
        if typingCounter == 0 {
            
            FireBaseTypingListener.saveTyping(typing: false, chatRoomId: chatId)
            
            
        }
        
    }
    
    
    func updateTypingIndicator(_ show: Bool) {
        
        
        subtitleLabel.text = show ? "typing..." : ""
    }
    
     //MARK: - UIScrollViewDelegate
     func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if refreshController.isRefreshing {
            
            if displayingMessagesCount < allLocalMessage.count {
                
                self.loadMoreMessages(maxNember: maxMessageNumber, minNumber: minMessageNumber)
                
                messagesCollectionView.reloadDataAndKeepOffset()
            }
            
            refreshController.endRefreshing()
            
        }
    }
    
    
     //MARK: - UpdateReadMessageStatus
    private func updatedMessage(_ localMessage: LocalMessage) {
        
        for index in 0 ..< mkMessages.count {
            let tempMessage = mkMessages[index]
            
            if localMessage.id == tempMessage.messageId {
               
                mkMessages[index].status = localMessage.status
                mkMessages[index].readDate = localMessage.readDate

                RealmManager.shared.saveToRealm(localMessage)
                
                if mkMessages[index].status ==  kREAD {
                    
                    self.messagesCollectionView.reloadData()
                    
                }
                
            }
            
        }
    }
    
    
    
 //MARK: - Helpers
   
    private func removeListener() {
        
        FireBaseTypingListener.shared.removeTypingListener()
        FirebaseMessageListener.shared.removeListener()
        
        
    }
    
    
    
    private func lastMessageDate() -> Date {
        
        let lastMessageDate = allLocalMessage.last?.date ?? Date()
        return Calendar.current.date(byAdding: .second, value: 1, to: lastMessageDate) ?? lastMessageDate
        
    }

    
     //MARK: - Gallery
    private func showImageGallery(camera: Bool) {
        self.gallery = GalleryController()
        self.gallery.delegate = self
        
        Config.tabsToShow = camera ? [.cameraTab ] : [.imageTab, .videoTab]
        Config.Camera.imageLimit = 1
        Config.initialTab = .imageTab
        Config.VideoEditor.maximumDuration = 30
        
        
        self.present(gallery, animated: true, completion: nil)
    }

     //MARK: - AudioMessages
    @objc func recordAudio() {
       
        
        switch longPressGesture.state {
        
        
        case .began:
            
            audioDuration = Date()
            audioFileName = Date().stringDate()
            AudioRecorder.shared.startRecording(fileName: audioFileName)
        
        
        
        case .ended:
            //stop recording
            AudioRecorder.shared.finishrecording()
            
            if fileExistAtPath(path: audioFileName + ".m4a") {
                
                let audioD = audioDuration.interval(ofComponent: .second, from: Date())
                
                messageSend(text: nil, photo: nil, video: nil, adio: audioFileName, location: nil, audioDuration: audioD)
            } else {
                
                print("no audio file")
                
            }
            audioFileName = ""
        
        @unknown default:
            print("unknown")
        }
    }
}

extension ChatViewController: GalleryControllerDelegate {
  
    
    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
     
        if images.count > 0 {
            images.first?.resolve(completion: { (image) in
                
                self.messageSend(text: nil, photo: image, video: nil, adio: nil, location: nil)
            })
        }
        
        
        controller.dismiss(animated: true, completion: nil)

    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
       
        print("selected video")
        
        self.messageSend(text: nil, photo: nil, video: video, adio: nil, location: nil)
        
        controller.dismiss(animated: true, completion: nil)

    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        controller.dismiss(animated: true, completion: nil)

    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
}
