//
//  FirebaseMessageListener.swift
//  Xapp
//
//  Created by Javad faghih on 10/20/1399 AP.
//

import Foundation
import Firebase
import FirebaseFirestoreSwift

class FirebaseMessageListener {
    
    static let shared = FirebaseMessageListener()
    var newChatListener: ListenerRegistration!
    var updatedChatListener: ListenerRegistration!
    
    private init() { }
    
    
    func listenForNewChats(_ documentId: String, collectionId:String, lastMessageDate: Date) {
        
        
        newChatListener = firebaseReference(.Message).document(documentId).collection(collectionId).whereField(kDATE, isGreaterThan: lastMessageDate).addSnapshotListener({ (querySnapshot, error) in
            
            guard let snapshot = querySnapshot else { return }
            
            for change in snapshot.documentChanges {
                
                if change.type == .added {
                    
                    let result = Result {
                        try? change.document.data(as: LocalMessage.self)
                    }
                    
                    switch result {
    
                    case .success(let messageObject):
                        if let message = messageObject {
                       
                            if message.senderId != User.currentID {
                                
                                RealmManager.shared.saveToRealm(message)

                            }
                            
                            
                        } else {
                            print("document does'n exist")
                        }
                    case .failure(let error):
                        print("error decoding local message", error.localizedDescription)
                    }
                    
                }
                
                
            }
            
        })
        
        
    }
    
    
    func listenForReadStatusChanges(_ documentId: String, collectionId: String, completion: @escaping (_ updatedMessage: LocalMessage) -> Void) {
        
        updatedChatListener = firebaseReference(.Message).document(documentId).collection(collectionId).addSnapshotListener({ (querySnapShot, error) in
            
            guard let snapshot = querySnapShot else { return }
            
            for change in snapshot.documentChanges {
                
                if change.type == .modified {
                    
                    let result = Result {
                        try? change.document.data(as: LocalMessage.self)
                    }
                    
                    switch result {
                    
        
                    case .success(let messageObject):
                        
                        
                        
                        if let message = messageObject {
                            completion(message)
                        } else {
                            
                            print("document does not exict in the chat")
                            
                        }
                    case .failure(let error):
                        print("error docoding loacal message \(error.localizedDescription)")
                    }
                    
                    
                    
                    
                }
                
            }
            
        })
        
    }
    
    
    func checkForOldChats(_ documentId: String, collectionId: String) {
        
        firebaseReference(.Message).document(documentId).collection(collectionId).getDocuments { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                debugPrint("no documents for old chats")
                return
            }
            
            var oldMessages = documents.compactMap { (queryDocumentSnapshot) -> LocalMessage? in
                return try? queryDocumentSnapshot.data(as: LocalMessage.self)
            }
            
            oldMessages.sort(by: { $0.date < $1.date })
            
            for message in oldMessages {
                
                RealmManager.shared.saveToRealm(message)
                
            }
        }
        
        
    }
    
    
     //MARK: - Add, Update, Delete
    
  
    func addMessage(_ message: LocalMessage, memberId: String) {
        
        do {
            let _ = try firebaseReference(.Message).document(memberId).collection(message.chatRoomId).document(message.id).setData(from: message)
        }
        catch {
            print("error saving Message...", error.localizedDescription)
            
            
        }
        
    }
    
    func addChannelMessage(_ message: LocalMessage, channel: Channel) {
        
        do {
            let _ = try firebaseReference(.Message).document(channel.id).collection(channel.id).document(message.id).setData(from: message)
        }
        catch {
            print("error saving Message...", error.localizedDescription)
            
            
        }
        
    }
    
    
     //MARK: - UpdateMessageStatus
    func updateMessageInFirebase(_ message: LocalMessage, memberIds: [String])  {
        
        let values = [kSTATUS : kREAD, kREADDATE : Date()] as [String : Any]
        
        for userId in memberIds {
            
            firebaseReference(.Message).document(userId).collection(message.chatRoomId).document(message.id).updateData(values)
            
            
        }
        
    }
    
    
    
    
   
    func removeListener() {
        self.newChatListener.remove()

        
        if updatedChatListener != nil {
                self.updatedChatListener.remove()
        }
        
    }
    
    
    
    
    
}

