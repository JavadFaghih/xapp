//
//  FileStorage.swift
//  Xapp
//
//  Created by Javad faghih on 10/7/1399 AP.
//

import UIKit
import FirebaseStorage
import ProgressHUD


let storage = Storage.storage()

class FileStorage {
    
     //MARK: - Images
    class func uploadImage(_ image: UIImage, directory: String, completion: @escaping(_ documentLink: String?) -> Void) {
        
        let storageREF = storage.reference(forURL: kFILEREFERENCE).child(directory)
        
        let imageData = image.jpegData(compressionQuality: 0.6)
        
        var task: StorageUploadTask!
        
        task = storageREF.putData(imageData!, metadata: nil, completion: { (metadata, error) in
            
            task.removeAllObservers()
            ProgressHUD.dismiss()
            
            if error != nil {
                
                print("error uploading image \(error!.localizedDescription)")
                return
            }
            storageREF.downloadURL { (url, error) in
                
                guard let downloadURL = url else {
                    completion(nil)
                    return }
                
                completion(downloadURL.absoluteString)
            }
        })
        
        task.observe(StorageTaskStatus.progress) { (snapshot) in
            var progress = snapshot.progress!.completedUnitCount / snapshot.progress!.totalUnitCount
            
            ProgressHUD.showProgress(CGFloat(progress))
        }
    }
    
    class func downloadImage(imageUrl: String, completion: @escaping(_ image: UIImage?) -> Void) {
       
        let imageFileName = fileNamefrom(fileURL: imageUrl)
        
        if fileExistAtPath(path: imageFileName) {
          // get it localy
            print("we have local image")

            if let contentsOfFile = UIImage(contentsOfFile: fileInDocumentsDirectory(fileName: imageFileName)) {
                
                completion(contentsOfFile)
            } else {
                print("couldn't convert local image")
                completion(UIImage(named: "avatar"))
            }
            
            
        } else {
            //download from firebase
            print("let gets from firebase")
            if imageUrl != "" {
                let documentUrl = URL(string: imageUrl)
                
                let downloadQueue = DispatchQueue(label: "ImageDownloadQueue")
                
                downloadQueue.async {
                    let data = NSData(contentsOf: documentUrl!)
                    
                    if data != nil {
                        //Save locally
                        FileStorage.saveFileLocally(fileData: data!, fileName: imageFileName)
                        
                        DispatchQueue.main.async {
                            completion(UIImage(data: data! as Data))
                        }
                        
                    } else {
                        print("no document in database")
                        completion(nil)
                    }
                }
            }
            
        }
        
    }
     //MARK: - Video
    class func uploadVideo(_ video: NSData, directory: String, completion: @escaping(_ videoLink: String?) -> Void) {
        
        let storageREF = storage.reference(forURL: kFILEREFERENCE).child(directory)
                
        var task: StorageUploadTask!
        
        task = storageREF.putData(video as Data, metadata: nil, completion: { (metadata, error) in
            
            task.removeAllObservers()
            ProgressHUD.dismiss()
            
            if error != nil {
                
                print("error uploading video \(error!.localizedDescription)")
                return
            }
            storageREF.downloadURL { (url, error) in
                
                guard let downloadURL = url else {
                    completion(nil)
                    return }
                
                completion(downloadURL.absoluteString)
            }
        })
        
        task.observe(StorageTaskStatus.progress) { (snapshot) in
            var progress = snapshot.progress!.completedUnitCount / snapshot.progress!.totalUnitCount
            
            ProgressHUD.showProgress(CGFloat(progress))
        }
    }
    
    class func downloadVideo(videoLink: String, completion: @escaping(_ isReadyToPlay: Bool,_ videoFileName: String) -> Void) {
       
        
        let videoUrl = URL(string: videoLink)
        let videoFileName = fileNamefrom(fileURL: videoLink) + ".mov"
        
        if fileExistAtPath(path: videoFileName) {
        
            completion(true, videoFileName)
            
        } else {
           
            let downloadqueue = DispatchQueue(label: "VideoDownloadQueue")
            
            downloadqueue.async {
                
                let data = NSData(contentsOf: videoUrl!)
                
                if data != nil {
                    
                    //Save locally
                    FileStorage.saveFileLocally(fileData: data!, fileName: videoFileName)
               
                    DispatchQueue.main.async {
                        
                      completion(true, videoFileName)
                        
                    }
                } else {
                    print("no document in database")
                }
            }
        }
    }
    
     //MARK: - Audio
    
    class func uploadAudio(_ audioFileName: String, directory: String, completion: @escaping(_ audioLink: String?) -> Void) {
        
        let fileName = audioFileName + ".m4a"
        
        let storageREF = storage.reference(forURL: kFILEREFERENCE).child(directory)
                
        var task: StorageUploadTask!
        if fileExistAtPath(path: fileName) {
            
            if let audioData = NSData(contentsOfFile: fileInDocumentsDirectory(fileName: fileName)) {
                
                task = storageREF.putData(audioData as Data, metadata: nil, completion: { (metadata, error) in
                    
                    task.removeAllObservers()
                    ProgressHUD.dismiss()
                    
                    if error != nil {
                        
                        print("error uploading audio \(error!.localizedDescription)")
                        return
                    }
                    storageREF.downloadURL { (url, error) in
                        
                        guard let downloadURL = url else {
                            completion(nil)
                            return }
                        
                        completion(downloadURL.absoluteString)
                    }
                })
                
                task.observe(StorageTaskStatus.progress) { (snapshot) in
                    var progress = snapshot.progress!.completedUnitCount / snapshot.progress!.totalUnitCount
                    
                    ProgressHUD.showProgress(CGFloat(progress))
                }
            } else {
                print("nothing to upload (audio)")
            }
        }
    }

    class func downloadAudio(audioLink: String, completion: @escaping(_ audioFileName: String) -> Void) {
       
        
        let audioFileName = fileNamefrom(fileURL: audioLink) + ".m4a"
        
        if fileExistAtPath(path: audioFileName) {
        
            completion(audioFileName)
            
        } else {
           
            let downloadqueue = DispatchQueue(label: "AudioDownloadQueue")
            
            downloadqueue.async {
                
                let data = NSData(contentsOf: URL(string: audioLink)!)
                
                if data != nil {
                    
                    //Save locally
                    FileStorage.saveFileLocally(fileData: data!, fileName: audioFileName)
               
                    DispatchQueue.main.async {
                        
                      completion(audioFileName)
                        
                    }
                } else {
                    print("no document in database audio")
                }
            }
        }
    }

    
    
    
    //MARK: - Save Locally
    class func saveFileLocally(fileData: NSData, fileName: String) {
       let docURL = getDocumentsURL().appendingPathComponent(fileName, isDirectory: false)
        fileData.write(to: docURL, atomically: true)
        
        
    }
}

//Helpers

func fileInDocumentsDirectory(fileName: String) -> String {

    return getDocumentsURL().appendingPathComponent(fileName).path
}

func getDocumentsURL() -> URL {

    return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last!
}

func fileExistAtPath(path: String) -> Bool {
    
    return FileManager.default.fileExists(atPath: fileInDocumentsDirectory(fileName: path))
}
