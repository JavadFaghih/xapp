//
//  FirebaseUserListener.swift
//  Xapp
//
//  Created by Javad faghih on 9/30/1399 AP.
//

import Foundation
import Firebase

class FireBaseUserlistener {
    static let shared = FireBaseUserlistener()

    private init () { }
    
    //MARK: - login
    func loginUserWithEmail(email: String, password: String, complation: @escaping(_ error: Error?, _ isEmailVerified: Bool) -> Void) {
        
        Auth.auth().signIn(withEmail: email, password: password) { (authDataresult, error) in
            
            if error == nil && authDataresult!.user.isEmailVerified {
              
                FireBaseUserlistener.shared.downloadUserFromFirebase(userID: authDataresult!.user.uid, email: email)
                
                complation(error, true)
            }else {
                print("Email is not verified...")
                complation(error, false)
            }
        }
    }
    
    //MARK: - register
    func registerUserWith(_ email: String, _ password: String, complation: @escaping (_ error: Error?) -> Void) {
        
        Auth.auth().createUser(withEmail: email, password: password) { (authDataResult, error) in
            
           complation(error)
            
            if error == nil {
                //send verification email
                authDataResult!.user.sendEmailVerification { (error) in
                    print("auth email send with error...")
                }
                
                //create a user and save it
                if authDataResult?.user != nil {
                    
                    let user = User(id: authDataResult!.user.uid, username: email, email: email, pushID: "", avatarLink: "", status: "Hey there I'm Using Xapp")
                    
                    saveUserlocaly(user: user)
                    self.saveUserToFirestore(user)
                }
            }
        }
    }
    
    //MARK: - Resend Link Methods
    
    func resetPasswordFor(email: String, completion: @escaping(_ error: Error?) -> Void) {
        
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            
            if email == nil {
             
                print("reset password Email sent successfuly")
            } else {
                completion(error)
            }
        }
    }
    
    func  resendVerificationEmail(email: String, completion: @escaping(_ error: Error?) -> Void) {
        
        Auth.auth().currentUser?.reload(completion: { (error) in
            
            Auth.auth().currentUser?.sendEmailVerification(completion: { (error) in
                completion(error)
            })
        })
    }
    
    func logOutCurrentUser(completion: @escaping(_ error: Error?) -> Void) {
       
        do {
          try  Auth.auth().signOut()
            
            userDefaults.removeObject(forKey: kCURRENTUSER)
            userDefaults.synchronize()
            
            completion(nil)
        } catch let error as NSError {
            completion(error)
        }
    }
    
    //MARK: - save users
    func saveUserToFirestore(_ user: User) {
        
        do {
            try firebaseReference(.User).document(user.id).setData(from: user)
            print("save data to firestore successfully")
        } catch {
            print(error.localizedDescription, "Adding user ")
        }
    }
    
    //MARK: download user from firebase
    func downloadUserFromFirebase(userID: String, email: String) {
        
        firebaseReference(.User).document(userID).getDocument { (querySnapshot, error) in
            guard let document = querySnapshot else {
                print("no user for document found")
                return
            }
            
            let result = Result {
                
                try document.data(as: User.self)
            }
                switch result {
                
                case .success(let userObject):
                    if let user = userObject {
                        self.saveUserToFirestore(user)
                    } else {
                        print("document does not exist")
                    }
                case .failure(let error):
                    print("error docoding user, \( error.localizedDescription)")
                }
        }
    }
    
    func downloadAllUsersFromFirebase(completion: @escaping(_ allUser: [User]) -> Void) {
        
        var users: [User] = []
        
        firebaseReference(.User).limit(to: 500).getDocuments { (querrysnapshot, error) in
            
            guard let document = querrysnapshot?.documents else {
                print("no documents in all users")
                return
            }
            
            let allUsers = document.compactMap { (querryDocumentSnapshot) -> User? in
                
                return try? querryDocumentSnapshot.data(as: User.self)
                
            }
            
            for user in allUsers {
                
                if User.currentID != user.id {
                    users.append(user)
                }
            }
            completion(users)
        }
    }
    
    func downloadUsersFromFirebase(withIds: [String], completion: @escaping(_ allUsers: [User]) -> Void) {
        
        var count = 0
        var userarray: [User] = []
        
        for userId in withIds {
            
            firebaseReference(.User).document(userId).getDocument { (querrySnapShot, error) in
                
                guard let document = querrySnapShot else {
                    
                    print("no document for user")
                    return
                }
                
                let user = try? document.data(as: User.self)
                
                userarray.append(user!)
                count += 1
                
                if count == withIds.count {
                    completion(userarray)
                }
            }
        }
    }

 //MARK: - Update
func updateUserInFirebase(_ user: User) {
    
    do {
        let _ = try firebaseReference(.User).document(user.id).setData(from: user)
    } catch {
        print(error.localizedDescription, "updating user...")
    }
    
    
    }
}
