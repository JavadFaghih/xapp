//
//  RecentChatListener.swift
//  Xapp
//
//  Created by Javad faghih on 10/13/1399 AP.
//

import Foundation
import Firebase

class FirebaseRecentListener {
    
   static let shared = FirebaseRecentListener()
    
    private init() { }
    
    
    func downloadrecentChatsFromFireStore(completion: @escaping(_ allrecents: [RecentChat]) -> Void) {
        
        firebaseReference(.Recent).whereField(kSENDERID, isEqualTo: User.currentID).addSnapshotListener { (snapshot, error) in
            
            var recentChat: [RecentChat] = []
           
            guard let documents = snapshot?.documents else { print("no documents for recent chats")
                return
            }
            
            let allrecents = documents.compactMap { (queryDocumentSnapshot) -> RecentChat? in
                return try? queryDocumentSnapshot.data(as: RecentChat.self)
            }

            for recent in allrecents {
                if recent.lastMessage != "" {
                    recentChat.append(recent)
                }
            }

            recentChat.sorted(by: {$0.date!  > $1.date!})
            completion(recentChat)
        }
        
        
        
    }
    func resetRecentCounter(chatRoomId: String) {
        
        firebaseReference(.Recent).whereField(kCHATROOMID, isEqualTo: chatRoomId).whereField(kSENDERID, isEqualTo: User.currentID).getDocuments { (querySnapshot, error) in
            
            
            guard let documents = querySnapshot?.documents else {
                print("no documents for recent")
                return
            }
            
            
            let allrecent = documents.compactMap { (queryDocumentSnapShot) -> RecentChat? in
                
                
                return try? queryDocumentSnapShot.data(as: RecentChat.self)
            }
            
            if allrecent.count > 0 {
                self.clearUnreadCounter(recent: allrecent.first!)
            }
            
        }
        
        
    }
    
    func updateRecents(chatRoomId: String, lastMessage: String) {
        
        firebaseReference(.Recent).whereField(kCHATROOMID, isEqualTo: chatRoomId).getDocuments { (querySnapshot, error) in
            
            guard let documents = querySnapshot?.documents else {
                print("no document for rercent update")
                return
            }
            
            
            let allRecents = documents.compactMap { (queryDocumentSnapshot) -> RecentChat? in
                return try? queryDocumentSnapshot.data(as: RecentChat.self)
            }
            
            for recentChat in allRecents {
                
                self.updateRecentWithNewMessage(recent: recentChat, lastMessage: lastMessage)
            }
        }
    }
    
    
    
    private func updateRecentWithNewMessage(recent: RecentChat, lastMessage: String) {
        
        var temprecent = recent
        
        if temprecent.senderId != User.currentID {
            
            temprecent.underCounter += 1
            
            
            
        }
        temprecent.lastMessage = lastMessage
        temprecent.date = Date()
        
        self.saveRecent(temprecent)
    }
    
    
    
    
    
    func clearUnreadCounter(recent: RecentChat) {
        
        var newRecent = recent
        newRecent.underCounter = 0
        self.saveRecent(newRecent)
        
    }
    
    
    
    func saveRecent(_ recent: RecentChat) {
        do {
      try firebaseReference(.Recent).document(recent.id).setData(from: recent)
        }
        catch {
            print("Error saving recent chat,", error.localizedDescription)
        }
        
    }
    
    func deleteRecent(_ recent: RecentChat) {
        
        firebaseReference(.Recent).document(recent.id).delete()
    
    }
    
    
    
    
}
