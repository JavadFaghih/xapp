//
//  FirebaseTypingListener.swift
//  Xapp
//
//  Created by Javad faghih on 10/22/1399 AP.
//

import Foundation
import Firebase


class FireBaseTypingListener {
    
    
    static let shared = FireBaseTypingListener()
    
    var typingListener: ListenerRegistration!
    
    private init() {
    }
    
    func cratetypingobserver(chatRoomId: String, completion: @escaping(_ isTyping: Bool)  -> Void ) {
        
        
        typingListener = firebaseReference(.Typing).document(chatRoomId).addSnapshotListener({ (snapshot, error) in
            
            guard  let snapshot = snapshot else { return }
            
            if snapshot.exists {
                
                for data in snapshot.data()! {
                    
                    if data.key != User.currentID {
                        
                        
                        completion(data.value as! Bool)
                    }
                    
                    
                }
                
            } else {
            
                completion(false)
                firebaseReference(.Typing).document(chatRoomId).setData([User.currentID : false])
                
            }
            
        })
        
    }
    
    class func saveTyping(typing: Bool, chatRoomId: String) {
        
        firebaseReference(.Typing).document(chatRoomId).updateData([User.currentID : typing])
        
        
    }
    
    func removeTypingListener() {
        
        self.typingListener.remove()
        
    }
    
    
}
