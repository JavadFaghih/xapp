//
//  FirebaseChannelListener.swift
//  Xapp
//
//  Created by Javad faghih on 10/28/1399 AP.
//

import Foundation
import Firebase

class FirebaseChannelListener {
    
    static let shared = FirebaseChannelListener()
    
    var channelListener: ListenerRegistration!

    private init() { }
    
     //MARK: - Fetching
    func downloadUserChannelsFromFirebase(completion: @escaping(_ allChannels: [Channel]) -> Void) {
        
        channelListener = firebaseReference(.Channel).whereField(kADMINID, isEqualTo: User.currentID).addSnapshotListener({ (querySnapShot, error) in
            
            guard let documents = querySnapShot?.documents else { print("no document for user channel")
                return
            }
            
            var allChannels = documents.compactMap { (queryDocumentsSnapshot) -> Channel? in
                
                return try? queryDocumentsSnapshot.data(as: Channel.self)
                
                
            }
            allChannels.sort(by: { $0.memberIds.count > $1.memberIds.count
            })
            completion(allChannels)

        })
        
        
        
    }
    
    
    func downloadSubscribedChannels(completion: @escaping(_ allChannels: [Channel]) -> Void) {
        
        channelListener = firebaseReference(.Channel).whereField(kMEMBERIDS, arrayContains: User.currentID).addSnapshotListener({ (querySnapShot, error) in
            
            guard let documents = querySnapShot?.documents else { print("no document for user channel")
                return
            }
            
            var allChannels = documents.compactMap { (queryDocumentsSnapshot) -> Channel? in
                
                return try? queryDocumentsSnapshot.data(as: Channel.self)
                
                
            }
            allChannels.sort(by: { $0.memberIds.count > $1.memberIds.count
            })
            completion(allChannels)

        })
        
        
        
    }
    
    func downloadAllChannels(completion: @escaping(_ allChannels: [Channel]) -> Void) {
        
    firebaseReference(.Channel).getDocuments { (querySnapShot, error) in
            
            guard let documents = querySnapShot?.documents else { print("no document for all channel")
                return
            }
            
            var allChannels = documents.compactMap { (queryDocumentsSnapshot) -> Channel? in
                
                return try? queryDocumentsSnapshot.data(as: Channel.self)
                
                
            }
        
        allChannels = self.removeSuscribedChannel(allChannels)
            allChannels.sort(by: { $0.memberIds.count > $1.memberIds.count
            })
            completion(allChannels)

        }
    }
    
    
     //MARK: - add Update Delete
    func saveChannel(_ channel: Channel) {
        do {
        let _ = try firebaseReference(.Channel).document(channel.id).setData(from: channel)
        } catch {
            
            print("error saving channel", error.localizedDescription)
        }
        
    }
    
    func deleteChannel(_ channel: Channel) {
        
        firebaseReference(.Channel).document(channel.id).delete()
        
        
    }
    
     //MARK: - Helpers
    func removeSuscribedChannel(_ allChannels: [Channel]) -> [Channel] {
        
        var newChannels: [Channel] = []
        
        for channel in allChannels {
            
            if !channel.memberIds.contains(User.currentID) {
                
                newChannels.append(channel)
                
            }
            
        }
        return newChannels
    }
    
    func removeChannelListener() {
        
        self.channelListener.remove()
        
        
    }
}
