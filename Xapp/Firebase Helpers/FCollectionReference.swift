//
//  FCollectionReference.swift
//  Xapp
//
//  Created by Javad faghih on 9/30/1399 AP.
//

import Foundation
import FirebaseFirestore


enum FCollectionReference: String {
    case User
    case Recent
    case Message
    case Typing
    case Channel
}

func firebaseReference(_ collectionReference: FCollectionReference) -> CollectionReference {
    
    
    return Firestore.firestore().collection(collectionReference.rawValue)
}
